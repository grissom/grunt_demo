'use strict';

module.exports = function(grunt) {

    // 项目配置
    grunt.initConfig({
        // 加载元数据
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>*/\n',
        // 合并任务配置
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true,
            },
            css: {
              // 源文件，数组，
                src: ['src/css/test1.css', 'src/css/test2.css'],
                // 目标文件, pkg.name 是定义在 package.json 文件中的 name
                dest: 'dest/<%= pkg.name %>.css'
            },
            js: { 
                options: {
                  // js 文件合并用 ';'分隔
                  separator: ';',
                 },
                src: ['src/js/test1.js', 'src/js/test2.js'],
                dest: 'dest/<%= pkg.name %>.js'
            },
        },
        // 压缩 css 文件
        cssmin: {
            css: {
                src: 'dest/<%= pkg.name %>.css',
                dest: 'dest/<%= pkg.name %>-min.css'
            }
        },
        // 压缩 js 文件
        uglify: {
            js: {
                src: 'dest/<%= pkg.name %>.js',
                dest: 'dest/<%= pkg.name %>.min.js'
            },
        },
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');  
    grunt.loadNpmTasks('grunt-css');
    // 指定默认任务.
    grunt.registerTask('default', ['concat','cssmin','uglify']);
};
